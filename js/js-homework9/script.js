let tabs = document.getElementById('tabs');
let tabContent = document.getElementById('tabContent');

for (let i = 0; i < tabContent.children.length; i++){
	tabs.children[i].dataset.index = i;
	if(i)
		tabContent.children[i].hidden = true;
}

tabs.onclick = e => {
	tabs.querySelector(".active").classList.toggle("active");
	tabContent.querySelector("li:not([hidden])").hidden = true;
	e.target.classList.toggle("active");
	tabContent.children[e.target.dataset.index].hidden = false;
}