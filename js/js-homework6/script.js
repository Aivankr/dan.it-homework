function filterBy(arr, dataType){
	if (dataType === 'null') {
		return arr.filter(el => el !== null);
	}else if (dataType === 'object'){
		return arr.filter(el => typeof el !== 'object' || el === null);
	}else{
		return arr.filter(el => typeof el !== dataType)
	}
}

let arr = [null, 22, [2, 3, "string", undefined], "aaa", "11", undefined, {name: 'Ivan'}];

let type = prompt("Enter type to delete from array", "null");
console.log(filterBy(arr, type));