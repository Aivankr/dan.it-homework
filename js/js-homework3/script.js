let firstNumber = prompt("Enter first number", "2");
let secondNumber = prompt("Enter second number", "2");

while (isNaN(firstNumber) || isNaN(secondNumber)){
	firstNumber = prompt("Enter correct first number", firstNumber);
	secondNumber = prompt("Enter correct second number", secondNumber);
}
let operation = prompt("Enter operation", "+");
let result = calc(+firstNumber, +secondNumber, operation);

if (result) {
	console.log(`Result of ${firstNumber} ${operation} ${secondNumber} is ${result}`);
}else{
	console.log('Wrong operation!')
}

function calc(a, b, op){
	switch(op){
		case "*":
			return (a*b);
		case "/":
			return (a/b);
		case "+":
			return (a+b);
		case "-":
			return (a-b);
		default:
			return false;
	}
}