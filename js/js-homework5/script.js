let name = prompt("What is your name?", "");
let surname = prompt("What is your surname?", "");

function createNewUser(name, surname){
	let birth = prompt("When is your birthday?(dd.mm.yyyy)", "");

	let newUser = {
		firstName: name,
		lastName: surname,
		birthday: birth,
		getLogin: function() {
			return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase()
		},
		getAge: function(){
			let birth = this.birthday.split(".");
			let birthDate = new Date(birth[2], birth[1], birth[0]);
			return Math.floor((Date.now() - birthDate)/(1000*60*60*24*365));
		},
		getPassword: function(){
			return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4);
		}
	};
	return newUser;
}

let user = createNewUser(name, surname);
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());