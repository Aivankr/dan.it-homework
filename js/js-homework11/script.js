let btns = document.getElementsByClassName('btn');

document.body.onkeypress = e => {
	let key = e.code.includes("Key") ? e.code.slice(3).toLowerCase() : e.code.toLowerCase();

	for(let i = 0; i < btns.length; i++){
		if(btns[i].innerText.toLowerCase() == key){
			btns[i].style.backgroundColor = "blue";
		}else{
			btns[i].style.backgroundColor = "";
		}
	}
}