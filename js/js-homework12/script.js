let container = document.getElementById('imgWrap');

for (let i = 0; i < container.children.length; i++){
	container.children[i].dataset.index = i;
	if(i !== 0)
		container.children[i].hidden = true;
}

let intervalId, timerIntervalId;

function startInterval(){
	timer.innerText = "10.00";
	timerIntervalId = setInterval(() => {
		timer.innerText = (timer.innerText - 0.01).toFixed(2);
	}, 10)

	intervalId = setInterval(() => {
		timer.innerText = "10.00";
		let curr = container.querySelector('img:not([hidden])');
		curr.hidden = true;
		if (curr === container.lastElementChild){
			container.firstElementChild.hidden = false;
		}else{
			container.children[+curr.dataset.index + 1].hidden = false;
		}
	}, 10000)
};

let stopBtn = document.createElement('button');
let continueBtn = document.createElement('button');
let timer = document.createElement('p');

stopBtn.innerText = "Прекратить";
continueBtn.innerText = "Возобновить";
continueBtn.disabled = true;

stopBtn.onclick = () => {
	clearInterval(timerIntervalId);
	clearInterval(intervalId);
	continueBtn.disabled = false;
	stopBtn.disabled = true;
}
continueBtn.onclick = () => {
	startInterval();
	continueBtn.disabled = true;
	stopBtn.disabled = false;
}

document.body.appendChild(timer);
document.body.appendChild(stopBtn);
document.body.appendChild(continueBtn);

startInterval();
