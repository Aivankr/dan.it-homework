let input = document.getElementById('priceInput');

input.onfocus = () => input.style.borderColor = "lime";
input.onblur = () => {
	if(!(+input.value < 0))
		input.style.borderColor = "";
}

let div = document.createElement('div');
let span = document.createElement('span');
let clsBtn = document.createElement('button');

let err = document.createElement('p');
err.innerText = "Please enter correct price";

input.onchange = () => {
	if(+input.value >= 0){
		err.remove();
		span.innerText = `Текущая цена: ${input.value}`;
		div.appendChild(span);
		clsBtn.innerText = 'X';
		clsBtn.onclick = () => {
			input.value = "";
			input.style.backgroundColor = "";
			clsBtn.parentElement.remove();
		}
		div.appendChild(clsBtn);
		document.body.insertBefore(div, document.body.children[0]);
		input.style.backgroundColor = 'lime';
	}else{
		input.style.borderColor = "red";
		input.style.backgroundColor = "";
		document.body.appendChild(err);
		if(clsBtn.parentElement)
			clsBtn.parentElement.remove();
	}
}


