let userNumber = +prompt("Enter your number", "5");

while((userNumber ^ 0) !== userNumber){
	userNumber = +prompt("Enter integer number", userNumber);
}

if (userNumber > 5){
	for (let i = 5; i < userNumber; i+=5){
		console.log(i);
	}
}else{
	console.log("Sorry, no numbers")
}

//Prime numbers task

console.log("Prime numbers:");

function isPrime(num){
	if(num < 2){
		return false;
	}else if(num == 2){
		return true;
	}else if(num % 2 === 0){
		return false;
	}

	let div = 3;
	while(div**2 <= num){
		if(num % div === 0){
			return false;
		}
		div += 2;
	}

	return true;
}

let first = +prompt("Enter the first number", "5"),
	second = +prompt("Enter the second number", "24");

while((first ^ 0) !== first || (second ^ 0) !== second){
	alert("Error! Numbers must be integer!")
	first = +prompt("Enter the first number", "5"),
	second = +prompt("Enter the second number", "24");
}

let m, n;

if (first > second){
	m = second;
	n = first;
}else{
	m = first;
	n = second;
}

for (let i = m; i < n; i++){
	if(isPrime(i)){
		console.log(i);
	}
}