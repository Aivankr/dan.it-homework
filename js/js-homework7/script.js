function displayInList(arr){
	let list = arr => {
		let ul = document.createElement('ul');
		if (Array.isArray(arr)){
			arr.map(el => {
				let li = document.createElement('li');
				if(el === null){
					li.innerText = `null`;
				}else if(typeof el === 'object'){
					li.appendChild(list(el))
				}else{
					li.innerText = `${el}`;
				}
				ul.appendChild(li);
			});
		}else{
			for (let key in arr){
				let li = document.createElement('li');
				if(arr[key] === null){
					li.innerText = `${key}: null`;
				}else if(typeof arr[key] === 'object'){
					li.innerText = `${key}: `;
					li.appendChild(list(arr[key]));
				}else{
					li.innerText = `${key}: ${arr[key]}`;
				}
				ul.appendChild(li);
			}
		}
		return ul;
	}
	document.body.appendChild(list(arr));
}

function timer(t){
	let time = document.createElement('p');
	time.innerText = t;
	document.body.appendChild(time);
	setInterval(() => --time.innerText, 1000);
	setTimeout(() => document.body.innerHTML = "", 10000);
}

let arr = [null, 22, [2, 3, "string", undefined, {object: 'inside', array: 'with', another: ['array', 'inside']}], "aaa", "11", undefined, {name: 'Ivan', age: 22, dog: null}];

timer(10);
displayInList(arr);