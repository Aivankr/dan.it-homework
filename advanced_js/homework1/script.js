function Hamburger(size, stuffing){
	try{
		if (!arguments.length) 
			throw new HamburgerExeption("No size given")
		
		if(arguments.length === 1)
			throw new HamburgerExeption("No stuffing given")

		if(size.type !== "size")
			throw new HamburgerExeption(`Invalid size ${size.name}`)

		if(stuffing.type !== "stuffing")
			throw new HamburgerExeption(`Invalid stuffing ${stuffing.name}`)

		this.size = size;
		this.stuffing = stuffing;
		this.toppings = [];
	}catch(e){
		console.log(`${e.name}: ${e.message}`)
	}
}

Hamburger.SIZE_SMALL = {
	type: "size",
	name: "SIZE_SMALL",
	price: 50,
	cal: 20
}
Hamburger.SIZE_LARGE = {
	type: "size",
	name: "SIZE_LARGE",
	price: 100,
	cal: 40
}
Hamburger.STUFFING_CHEESE = {
	type: "stuffing",
	name: "STUFFING_CHEESE",
	price: 10,
	cal: 20
}
Hamburger.STUFFING_SALAD = {
	type: "stuffing",
	name: "STUFFING_SALAD",
	price: 20,
	cal: 5
}
Hamburger.STUFFING_POTATO = {
	type: "stuffing",
	name: "STUFFING_POTATO",
	price: 15,
	cal: 10
}
Hamburger.TOPPING_MAYO = {
	type: "topping",
	name: "TOPPING_MAYO",
	price: 20,
	cal: 5
}
Hamburger.TOPPING_SPICE = {
	type: "topping",
	name: "TOPPING_SPICE",
	price: 15,
	cal: 0
}

Hamburger.prototype.addTopping = function(topping){
	try{
		if(topping.type !== "topping")
			throw new HamburgerExeption(`Invalid topping ${topping.name}`)

		if(!this.toppings.includes(topping)){
			this.toppings.push(topping);
		}else{
			throw new HamburgerExeption(`Duplicate topping ${topping.name}`)
		}
	}catch(e){
		console.log(`${e.name}: ${e.message}`)
	}
}

Hamburger.prototype.removeTopping = function(topping){
	try{
		if(topping.type !== "topping")
			throw new HamburgerExeption(`Invalid topping ${topping.name}`)

		let toppings = this.toppings;
		if(toppings.includes(topping)){
			toppings.splice(toppings.indexOf(topping), 1);
		}else{
			throw new HamburgerExeption(`Hamburger doesn't have ${topping.name}`)
		}
	}catch(e){
		console.log(`${e.name}: ${e.message}`)
	}
}

Hamburger.prototype.getToppings = function(){
	return this.toppings;
}

Hamburger.prototype.getSize = function(){
	return this.size;
}

Hamburger.prototype.getStuffing = function(){
	return this.stuffing;
}

Hamburger.prototype.calculatePrice = function(){
	let price = this.size.price + this.stuffing.price;
	for(let i = 0; i < this.toppings.length; i++)
		price += this.toppings[i].price;
	return price;
}

Hamburger.prototype.calculateCalories = function(){
	let calories = this.size.cal + this.stuffing.cal;
	for(let i = 0; i < this.toppings.length; i++)
		calories += this.toppings[i].cal;
	return calories;
}

function HamburgerExeption(message){
	this.message = message
	this.name = "HamburgerExeption"
}


let hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_POTATO)

hamburger.addTopping(Hamburger.TOPPING_SPICE)
hamburger.addTopping(Hamburger.TOPPING_MAYO)
hamburger.removeTopping(Hamburger.TOPPING_SPICE)

console.log("Toppings added: ", hamburger.getToppings())
console.log("Hamburger size: ", hamburger.getSize())
console.log("Hamburger stuffing: ", hamburger.getStuffing())
console.log("Hamburger price: ", hamburger.calculatePrice())
console.log("Hamburger calories: ", hamburger.calculateCalories())