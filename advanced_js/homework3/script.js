let table = document.createElement('table')
table.className = "table"

for(let i = 0; i < 30; i++){
	let tr = document.createElement('tr')
	tr.className = "row"
	for(let j = 0; j < 30; j++){
		let td = document.createElement('td')
		td.className = "cell"
		tr.appendChild(td)
	}
	table.appendChild(tr)
}

document.body.appendChild(table)

document.body.onclick = (e) => {
	if (e.target.classList.contains("cell")){
		e.target.classList.toggle("checked")
	}else if(e.target === document.body){
		document.querySelector(".table").classList.toggle("inverted")
	}
}