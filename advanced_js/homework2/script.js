class Hamburger{
	constructor(size, stuffing){
		try{
			if (!arguments.length) 
				throw new HamburgerExeption("No size given")
			
			if(arguments.length === 1)
				throw new HamburgerExeption("No stuffing given")

			if(size.type !== "size")
				throw new HamburgerExeption(`Invalid size ${size.name}`)

			if(stuffing.type !== "stuffing")
				throw new HamburgerExeption(`Invalid stuffing ${stuffing.name}`)

			this._size = size;
			this._stuffing = stuffing;
			this._toppings = [];
		}catch(e){
			console.log(`${e.name}: ${e.message}`)
		}
	}

	set topping(topping){
		try{
			if(topping.type !== "topping")
				throw new HamburgerExeption(`Invalid topping ${topping.name}`)

			if(!this._toppings.includes(topping)){
				this._toppings.push(topping);
			}else{
				throw new HamburgerExeption(`Duplicate topping ${topping.name}`)
			}
		}catch(e){
			console.log(`${e.name}: ${e.message}`)
		}
	}

	removeTopping(topping){
		try{
			if(topping.type !== "topping")
				throw new HamburgerExeption(`Invalid topping ${topping.name}`)

			let toppings = this._toppings;
			if(toppings.includes(topping)){
				toppings.splice(toppings.indexOf(topping), 1);
			}else{
				throw new HamburgerExeption(`Hamburger doesn't have ${topping.name}`)
			}
		}catch(e){
			console.log(`${e.name}: ${e.message}`)
		}
	}

	get toppings(){
		return this._toppings;
	}

	get size(){
		return this._size;
	}

	get stuffing(){
		return this._stuffing;
	}

	calculatePrice(){
		let price = this._size.price + this._stuffing.price;
		for(let i = 0; i < this._toppings.length; i++)
			price += this._toppings[i].price;
		return price;
	}

	calculateCalories(){
		let calories = this._size.cal + this._stuffing.cal;
		for(let i = 0; i < this._toppings.length; i++)
			calories += this._toppings[i].cal;
		return calories;
	}

}

Hamburger.SIZE_SMALL = {
	type: "size",
	name: "SIZE_SMALL",
	price: 50,
	cal: 20
}
Hamburger.SIZE_LARGE = {
	type: "size",
	name: "SIZE_LARGE",
	price: 100,
	cal: 40
}
Hamburger.STUFFING_CHEESE = {
	type: "stuffing",
	name: "STUFFING_CHEESE",
	price: 10,
	cal: 20
}
Hamburger.STUFFING_SALAD = {
	type: "stuffing",
	name: "STUFFING_SALAD",
	price: 20,
	cal: 5
}
Hamburger.STUFFING_POTATO = {
	type: "stuffing",
	name: "STUFFING_POTATO",
	price: 15,
	cal: 10
}
Hamburger.TOPPING_MAYO = {
	type: "topping",
	name: "TOPPING_MAYO",
	price: 20,
	cal: 5
}
Hamburger.TOPPING_SPICE = {
	type: "topping",
	name: "TOPPING_SPICE",
	price: 15,
	cal: 0
}

class HamburgerExeption{
	constructor(message){
		this.message = message
		this.name = "HamburgerExeption"
	}
}


let hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_POTATO)

hamburger.topping = Hamburger.TOPPING_SPICE
hamburger.topping = Hamburger.TOPPING_MAYO
hamburger.removeTopping(Hamburger.TOPPING_SPICE)

console.log("Toppings added: ", hamburger.toppings)
console.log("Hamburger size: ", hamburger.size)
console.log("Hamburger stuffing: ", hamburger.stuffing)
console.log("Hamburger price: ", hamburger.calculatePrice())
console.log("Hamburger calories: ", hamburger.calculateCalories())